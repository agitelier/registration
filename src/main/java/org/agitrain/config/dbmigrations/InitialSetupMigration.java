package org.agitrain.config.dbmigrations;

import org.agitrain.domain.Authority;
import org.agitrain.domain.User;
import org.agitrain.security.AuthoritiesConstants;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.time.Instant;

/**
 * Creates the initial database setup.
 */
@ChangeLog(order = "001")
public class InitialSetupMigration {

    @ChangeSet(order = "01", author = "initiator", id = "01-addAuthorities")
    public void addAuthorities(MongoTemplate mongoTemplate) {
        Authority adminAuthority = new Authority();
        adminAuthority.setName(AuthoritiesConstants.ADMIN);
        Authority userAuthority = new Authority();
        userAuthority.setName(AuthoritiesConstants.USER);
        Authority profAuthority = new Authority();
        profAuthority.setName(AuthoritiesConstants.Professeur);
        Authority etudAuthority = new Authority();
        etudAuthority.setName(AuthoritiesConstants.Etudiant);
        
        
        mongoTemplate.save(profAuthority);
        mongoTemplate.save(etudAuthority);
        mongoTemplate.save(adminAuthority);
        mongoTemplate.save(userAuthority);
    }
}
