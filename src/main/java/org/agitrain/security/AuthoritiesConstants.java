package org.agitrain.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";
    
    public static final String Professeur = "ROLE_PROFESSEUR";

    public static final String Etudiant = "ROLE_ETUDIANT";



    private AuthoritiesConstants() {
    }
}
