package org.agitrain.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import org.agitrain.kcclient.ApiClient;
import org.agitrain.kcclient.api.RealmsAdminApi;
import org.agitrain.kcclient.api.model.RealmRepresentation;
import org.agitrain.kcclient.auth.HttpBearerAuth;
import org.agitrain.security.SecurityUtils;
import org.agitrain.web.rest.kc.KeycloakConfigStatus;
import io.github.jhipster.web.util.PaginationUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing global OIDC logout.
 */
@RestController
@RequestMapping("/api")
public class KeycloakConfigResource {
    @Autowired
    RestTemplate restTemplate;

    private final ClientRegistration registration;

    public KeycloakConfigResource(ClientRegistrationRepository registrations) {
        this.registration = registrations.findByRegistrationId("oidc");
    }

    @GetMapping("/keycloakConfigStatus")
    public ResponseEntity<List<KeycloakConfigStatus>> getAllMissions() {
        RealmRepresentation rr = getRealmRepresentation();
        List<KeycloakConfigStatus> items = new ArrayList<>();
        KeycloakConfigStatus status;

        items.add(new KeycloakConfigStatus());
        status = items.get(0);
        status.setId(RealmRepresentation.JSON_PROPERTY_LOGIN_WITH_EMAIL_ALLOWED);
        status.setStatus(rr.getLoginWithEmailAllowed());
        status.setTitle("Email as login");
        status.setType("boolean");
        status.setDescription("Email as login");

        items.add(new KeycloakConfigStatus());
        status = items.get(1);
        status.setId(RealmRepresentation.JSON_PROPERTY_REGISTRATION_ALLOWED);
                status.setStatus(rr.getRegistrationAllowed());
                status.setTitle("User Registration Allowed");
                status.setType("boolean");
                status.setDescription("User Registration Allowed");

        items.add(new KeycloakConfigStatus());
        status = items.get(2);
        status.setId(RealmRepresentation.JSON_PROPERTY_REGISTRATION_EMAIL_AS_USERNAME);
                status.setStatus(rr.getRegistrationEmailAsUsername());
                status.setTitle("Email as username");
                status.setType("boolean");
                status.setDescription("Email as username");

        items.add(new KeycloakConfigStatus());
        status = items.get(3);
        status.setId(RealmRepresentation.JSON_PROPERTY_RESET_PASSWORD_ALLOWED);
                status.setStatus(rr.getResetPasswordAllowed());
                status.setTitle("Reset password allowed");
                status.setType("boolean");
                status.setDescription("Reset password allowed");

        items.add(new KeycloakConfigStatus());
        status = items.get(4);
        status.setId(RealmRepresentation.JSON_PROPERTY_VERIFY_EMAIL);
                status.setStatus(rr.getVerifyEmail());
                status.setTitle("Verify email");
                status.setType("boolean");
                status.setDescription("Verify email");

        items.add(new KeycloakConfigStatus());
        status = items.get(5);
        status.setId(RealmRepresentation.JSON_PROPERTY_BRUTE_FORCE_PROTECTED);
                status.setStatus(rr.getBruteForceProtected());
                status.setTitle("Brute Force Protected");
                status.setType("boolean");
                status.setDescription("Brute Force Protected");        

        Pageable pageable = PageRequest.of(0, items.size());
        Page<KeycloakConfigStatus> page = new PageImpl<>(items, pageable, items.size());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);

        return ResponseEntity.ok().headers(headers).body(items);
    }

    @GetMapping("/getParameter")
    public ResponseEntity<KeycloakConfigStatus> getParameter(@RequestParam String param) {
        RealmRepresentation rr = getRealmRepresentation();
        KeycloakConfigStatus status = new KeycloakConfigStatus();

        switch (param) {
            case "loginWithEmailAllowed":
                status.setId("loginWithEmailAllowed");
                status.setStatus(rr.getLoginWithEmailAllowed());
                status.setTitle("Login with email");
                status.setDescription("Login with email");
                break;

            case "registrationAllowed":
                status.setId("registrationAllowed");
                status.setStatus(rr.getRegistrationAllowed());
                status.setTitle("User Registration Allowed");
                status.setDescription("User Registration Allowed");
                break;

            case "registrationEmailAsUsername":
                status.setId("registrationEmailAsUsername");
                status.setStatus(rr.getRegistrationEmailAsUsername());
                status.setTitle("Email as username");
                status.setDescription("Email as username");
                break;

            case "resetPasswordAllowed":
                status.setId("resetPasswordAllowed");
                status.setStatus(rr.getResetPasswordAllowed());
                status.setTitle("Reset password allowed");
                status.setDescription("Reset password allowed");
                break;

            case "verifyEmail":
                status.setId("verifyEmail");
                status.setStatus(rr.getVerifyEmail());
                status.setTitle("Verify email");
                status.setDescription("Verify email");
                break;

            case "bruteForceProtected":
                status.setId("bruteForceProtected");
                status.setStatus(rr.getBruteForceProtected());
                status.setTitle("Brute Force Protected");
                status.setDescription("Brute Force Protected");
                break;  

            default:
                break;
        }
        return ResponseEntity.ok().body(status);
    }

    @PutMapping("/setParameter")
    public HttpStatus setParameter(@Valid @RequestBody KeycloakConfigStatus status) {
        RealmRepresentation rr = getRealmRepresentation();
        switch (status.getId()) {
            case RealmRepresentation.JSON_PROPERTY_LOGIN_WITH_EMAIL_ALLOWED:
                rr.setLoginWithEmailAllowed(status.isStatus());
                break;
            case RealmRepresentation.JSON_PROPERTY_REGISTRATION_ALLOWED:
                rr.setRegistrationAllowed(status.isStatus());
                break;
            case RealmRepresentation.JSON_PROPERTY_REGISTRATION_EMAIL_AS_USERNAME:
                rr.setRegistrationEmailAsUsername(status.isStatus());
                break;
            case RealmRepresentation.JSON_PROPERTY_RESET_PASSWORD_ALLOWED:
                rr.setResetPasswordAllowed(status.isStatus());
                break;
            case RealmRepresentation.JSON_PROPERTY_VERIFY_EMAIL:
                rr.setVerifyEmail(status.isStatus());
                break;
            case RealmRepresentation.JSON_PROPERTY_BRUTE_FORCE_PROTECTED:
                rr.setBruteForceProtected(status.isStatus());
                break;

            /*case RealmRepresentation.JSON_PROPERTY_LOGIN_THEME:
                rr.setLoginTheme(status.isStatus());
                break;*/
            // setLoginTheme set un boolean mais les themes ne sont pas true/false. Utiliser le champ "Title" maybe.

            default:
                return HttpStatus.BAD_REQUEST;
        }
        putRealmRepresentation(rr);
        return HttpStatus.OK;
    }

    /*@PutMapping("/setParameter")
    public HttpStatus setParameter(@RequestParam String param, @RequestParam String value) {
        RealmRepresentation rr = getRealmRepresentation();
        switch (param) {
            case "loginWithEmailAllowed":
                switch (value) {
                    case "true":
                        rr.setLoginWithEmailAllowed(true);
                        break;
                    case "false":
                        rr.setLoginWithEmailAllowed(false);
                        break;
                    default:
                        break;
                }
                break;

            case "setRegistrationAllowed":
                switch (value) {
                    case "true":
                        rr.setRegistrationAllowed(true);
                        break;
                    case "false":
                        rr.setRegistrationAllowed(false);
                        break;
                    default:
                        break;
                }
                break;
        }
        putRealmRepresentation(rr);
        return HttpStatus.OK;
    }*/

    @PutMapping("/keycloakConfigStatus")
    public ResponseEntity<String> putAllMissions(RequestEntity<String> str) {
        HttpHeaders headers = str.getHeaders();
        String body = str.getBody();
        System.out.println("Le requête contient : " + str.toString());
       return ResponseEntity.ok().headers(headers).body(body);
    }

    /**
     * Temporary endpoint to demonstrate a user obtaining their email address.
     * @return ur email
     */
    @GetMapping("/whatsMyEmail")
    public ResponseEntity<Optional<String>> whatsMyEmail() {
        return ResponseEntity.ok().body(SecurityUtils.getCurrentUserEmail());
    }

    /**
     * Temporary endpoint for dumping Keycloak's running config
     * @return a very long String
     */
    @GetMapping("/dumpKeycloakConfig")
    public ResponseEntity<String> dumpKeycloakConfig() {
        return ResponseEntity.ok().body(getRealmRepresentation().toString());
    }

    private String loginAdminOnMasterRealm() {
        //TODO ugly stuff, need some cache, some refresh, some config for admin username
        String tokenEnpoint =  registration.getProviderDetails().getTokenUri();
        tokenEnpoint = StringUtils.replace(tokenEnpoint, "jhipster", "master");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("client_id", "admin-cli");
        map.add("grant_type", "password");
        map.add("username", "admin");
        map.add("password", "admin");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<AccessTokenResponse> response = restTemplate.postForEntity(tokenEnpoint, request , AccessTokenResponse.class);

        return response.getBody().getAccesToken();
    }

    private RealmRepresentation getRealmRepresentation() {
        RealmsAdminApi realmsAdminApi = getRealmsAdminApi();
        return realmsAdminApi.realmGet("jhipster");
    }

    private void putRealmRepresentation(RealmRepresentation realmRepresentation) {
        RealmsAdminApi realmsAdminApi = getRealmsAdminApi();
        realmsAdminApi.realmPut("jhipster", realmRepresentation);
    }

    private RealmsAdminApi getRealmsAdminApi() {
        String token = loginAdminOnMasterRealm();

        String authUrl = registration.getProviderDetails().getAuthorizationUri();
        String basePath = StringUtils.substringBefore(authUrl, "auth/realms/");
        String keycloakApisPath = basePath + "auth/admin/realms";

        ApiClient apiClient = new ApiClient();
        ((HttpBearerAuth)apiClient.getAuthentication("access_token")).setBearerToken(token);
        apiClient.setBasePath(keycloakApisPath);
        RealmsAdminApi realmsAdminApi = new RealmsAdminApi(apiClient);
        return realmsAdminApi;
    }

}
