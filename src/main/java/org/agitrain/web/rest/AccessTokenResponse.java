package org.agitrain.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class AccessTokenResponse {
    @JsonProperty("access_token")
    protected String accesToken;

    @JsonProperty("expires_in")
    protected long expiresIn;

    @JsonProperty("refresh_expires_in")
    protected long refreshExpiresIn;

    @JsonProperty("refresh_token")
    protected String refreshToken;

    @JsonProperty("token_type")
    protected String tokenType;

    @JsonProperty("id_token")
    protected String idToken;

    @JsonProperty("not-before-policy")
    protected int notBeforePolicy;

    @JsonProperty("session_state")
    protected String sessionState;

    protected Map<String, Object> otherClaims = new HashMap<>();

    @JsonProperty("scope")
    protected String scope;


    public String getAccesToken() {
        return accesToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public long getRefreshExpiresIn() {
        return refreshExpiresIn;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getIdToken() {
        return idToken;
    }

    public int getNotBeforePolicy() {
        return notBeforePolicy;
    }

    public String getSessionState() {
        return sessionState;
    }

    public Map<String, Object> getOtherClaims() {
        return otherClaims;
    }

    public String getScope() {
        return scope;
    }

}
