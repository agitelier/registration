/**
 * Data Access Objects used by WebSocket services.
 */
package org.agitrain.web.websocket.dto;
