import { faTemperatureHigh } from '@fortawesome/free-solid-svg-icons';
import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import KeycloakConfigService from './keycloakconfig.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class KeycloakConfig extends Vue {
  @Inject('keycloakConfigService') private keycloakConfigService: () => KeycloakConfigService;
  private kcconfigs = '';

  public mounted(): void {
    this.init();
  }

  public init(): void {
    this.keycloakConfigService()
      .getAllConfigs()
      .then(response => {
        this.kcconfigs = response.data;
      });
  }

  public changeValue(name, value): void {
    this.keycloakConfigService()
      .setParameter(name, !value)
      .then(() => {
        this.init();
      });
  }

  public getId(element): string {
    return '"' + element.id + '"';
  }

  public getDynamicTranslation(element): string {
    return 'keycloakconfig.table.rownames.' + element.id;
  }

  public insertBasicButtonStyle(): string {
    return 'btn btn-sm btn-block text-uppercase font-weight-bold w-40';
  }

  public logToConsole(status): void {
    console.log(status);
  }
}
