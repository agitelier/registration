import axios, { AxiosPromise } from 'axios';
import Vue from 'vue';
import Component from 'vue-class-component';

@Component
export default class KeycloakConfigService extends Vue {
  public getAllConfigs(): AxiosPromise<any> {
    return axios.get('api/keycloakConfigStatus');
  }

  public getParameter(param: string): AxiosPromise<any> {
    return axios.get('api/getParameter?param=' + param);
  }

  public setParameter(param: string, value: any): AxiosPromise<any> {
    return axios.put('api/setParameter', { id: param, status: value });
  }

  public dumpConfigs(): AxiosPromise<any> {
    return axios.get('api/dumpKeycloakConfig');
  }
}
