import Component from 'vue-class-component';
import { Inject, Vue } from 'vue-property-decorator';
import LoginService from '@/account/login.service';
import AccountService from '@/account/account.service';

@Component
export default class Home extends Vue {
  @Inject('loginService')
  private loginService: () => LoginService;
  @Inject('accountService')
  private accountService: () => AccountService;

  private hasAnyAuthorityValue = false;

  // jhcc-custom
  public openLogin(): void {
    if (this.$store.getters.activeProfiles.includes('oauth2')) {
      this.loginService().login();
    } else {
      this.loginService().openLogin((<any>this).$root);
    }
  }

  public hasAnyAuthority(authorities: any): boolean {
    this.accountService()
      .hasAnyAuthorityAndCheckAuth(authorities)
      .then(value => {
        this.hasAnyAuthorityValue = value;
      });
    return this.hasAnyAuthorityValue;
  }

  public get authenticated(): boolean {
    return this.$store.getters.authenticated;
  }

  public get username(): string {
    return this.$store.getters.account ? this.$store.getters.account.login : '';
  }
}
